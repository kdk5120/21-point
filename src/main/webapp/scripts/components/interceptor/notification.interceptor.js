 'use strict';

angular.module('21PointsApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-21PointsApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-21PointsApp-params')});
                }
                return response;
            }
        };
    });
