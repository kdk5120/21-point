/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.lesson.web.rest.dto;
